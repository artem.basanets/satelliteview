using System;
using Newtonsoft.Json;

namespace SatelliteView
{
    public class Tle
    {
        [JsonProperty("@id")]
        public string Id { get; set; }

        [JsonProperty("@type")]
        public string Type { get; set; }
        public int satelliteId { get; set; }
        public string name { get; set; }
        public DateTime date { get; set; }
        public string line1 { get; set; }
        public string line2 { get; set; }
    }

    public class Position
    {
        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }
        public double r { get; set; }
        public string unit { get; set; }
    }

    public class Velocity
    {
        public double x { get; set; }
        public double y { get; set; }
        public double z { get; set; }
        public double r { get; set; }
        public string unit { get; set; }
    }

    public class Vector
    {
        public string reference_frame { get; set; }
        public Position position { get; set; }
        public Velocity velocity { get; set; }
    }

    public class Geodetic
    {
        public double latitude { get; set; }
        public double longitude { get; set; }
        public double altitude { get; set; }
    }

    public class Parameters
    {
        public DateTime date { get; set; }
        public int satelliteId { get; set; }
    }

    public class Satellite
    {
        [JsonProperty("@context")]
        public string Context { get; set; }

        [JsonProperty("@id")]
        public string Id { get; set; }

        [JsonProperty("@type")]
        public string Type { get; set; }
        public Tle tle { get; set; }
        public string algorithm { get; set; }
        public Vector vector { get; set; }
        public Geodetic geodetic { get; set; }
        public Parameters parameters { get; set; }
    }
}

