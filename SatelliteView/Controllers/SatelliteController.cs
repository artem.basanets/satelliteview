﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using RestSharp;

namespace SatelliteView.Controllers
{
    [ApiController]
    [Route("[controller]")]
    public class SatelliteController : ControllerBase
    {
        private readonly ILogger<SatelliteController> _logger;

        public SatelliteController(ILogger<SatelliteController> logger)
        {
            _logger = logger;
        }

        [HttpGet]
        public Satellite Get()
        {
            var client = new RestClient("https://tle.ivanstanojevic.me/api/tle/25544/propagate");
            client.Timeout = -1;
            var request = new RestRequest(Method.GET);
            IRestResponse response = client.Execute(request);
            return Deserialise(response.Content);
        }

        private Satellite Deserialise(string nasaData)
        {
            return JsonConvert.DeserializeObject<Satellite>(nasaData);
        }
    }
}
