import { Component, Inject, ViewChild } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, interval } from 'rxjs';
import { Satellite } from '../models/satellite.model'

@Component({
  selector: 'app-fetch-data',
  templateUrl: './fetch-data.component.html'
})

export class FetchDataComponent {
  public satellite: Satellite;
  @ViewChild('map', { static: false }) map;

  constructor(http: HttpClient, @Inject('BASE_URL') baseUrl: string) {
    interval(1000).subscribe(x => {
      http.get<Satellite>(baseUrl + 'satellite').subscribe(result => {
        this.satellite = result;
        console.log(result);
        if (this.map) {
          this.map.markerUpdate(this.satellite.geodetic);
        }        
      }, error => console.error(error));
    });
  }
}

