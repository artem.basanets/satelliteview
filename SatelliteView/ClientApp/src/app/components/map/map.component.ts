import { Component, AfterViewInit, Input } from '@angular/core';
import * as L from 'leaflet';
import { Satellite, Geodetic } from '../../models/satellite.model';

@Component({
  selector: 'app-map',
  templateUrl: './map.component.html',
  styleUrls: ['./map.component.css']
})
export class MapComponent implements AfterViewInit {

  private map: L.Map;

  @Input()
  satellite: Satellite;

  private marker: L.Marker;

  private centroid: L.LatLngExpression = [0, 0];
  
  constructor() { }

  private initMap(): void {
    this.map = L.map('map', {
      center: this.centroid,
      zoom: 2
    });

    const tiles = L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
      maxZoom: 18,
      minZoom: 2,
      attribution: '&copy; <a href="http://www.openstreetmap.org/copyright">OpenStreetMap</a>'
    });

    let zaryaIcon = L.icon({
      iconUrl: 'https://toppng.com/uploads/preview/eat-play-do-icon-map-marker-115548254600u9yjx6qhj.png',
      iconSize: [64, 64],
      iconAnchor: [32, 32],
    });

    this.marker = L.marker([this.satellite.geodetic.latitude, this.satellite.geodetic.longitude], { icon: zaryaIcon }).addTo(this.map);

    tiles.addTo(this.map);
  }

  ngAfterViewInit(): void {
    this.initMap();
  }

  public markerUpdate(geo: Geodetic): void {
    this.marker.setLatLng([geo.latitude, geo.longitude])
  }

}
