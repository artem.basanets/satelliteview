﻿import { Component, Inject } from '@angular/core';

export interface Tle {
  id: string;
  type: string;
  satelliteId: number;
  name: string;
  date: Date;
  line1: string;
  line2: string;
}

export interface Position {
  x: number;
  y: number;
  z: number;
  r: number;
  unit: string;
}

export interface Velocity {
  x: number;
  y: number;
  z: number;
  r: number;
  unit: string;
}

export interface Vector {
  reference_frame: string;
  position: Position;
  velocity: Velocity;
}

export interface Geodetic {
  latitude: number;
  longitude: number;
  altitude: number;
}

export interface Parameters {
  date: Date;
  satelliteId: number;
}

export class Satellite {
  context: string;
  id: string;
  type: string;
  tle: Tle;
  algorithm: string;
  vector: Vector;
  geodetic: Geodetic;
  parameters: Parameters;
}

